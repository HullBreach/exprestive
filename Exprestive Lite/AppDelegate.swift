//
//  AppDelegate.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/8/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	let siteId = "2c9d288b0168689519b8cef9fc4f0145"

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

		let appSettings = MMAppSettings()
		appSettings.siteId = siteId
		MMSDK.sharedInstance().initialize(with: appSettings, with: nil)
		MMSDK.setLogLevel(MMLogLevel.error)

		if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
			Globals.managedContext = appDelegate.persistentContainer.viewContext
		}
		
		return true
    }

	func applicationWillResignActive(_ application: UIApplication) {

	}

	func applicationDidEnterBackground(_ application: UIApplication) {

	}

	func applicationWillEnterForeground(_ application: UIApplication) {

	}

	func applicationDidBecomeActive(_ application: UIApplication) {

	}

	func applicationWillTerminate(_ application: UIApplication) {

		self.saveContext()
	}

	// MARK: - Core Data stack

	lazy var persistentContainer: NSPersistentContainer = {

		let container = NSPersistentContainer(name: "Exprestive")
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as NSError? {

				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		})
		return container
	}()

	// MARK: - Core Data Saving support

	func saveContext () {
		let context = persistentContainer.viewContext
		if context.hasChanges {
			do {
				try context.save()
			} catch {

				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
		}
	}

}

