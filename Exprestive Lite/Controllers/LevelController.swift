//
//  ViewController.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/8/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import AVFoundation
import Photos
import SpriteKit
import UIKit
import Vision

class LevelController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, MMInlineDelegate {

    @IBOutlet weak var viewBlur: UIVisualEffectView!
    @IBOutlet weak var adContainer: UIView!

	@IBOutlet weak var viewFace: ViewFace!

    @IBOutlet weak var btnClose: UIButton!
    
    @IBAction func clickedBtnClose(_ sender: UIButton) {

        dismiss(animated: true, completion: nil)
    }

	private var faceRequest: VNDetectFaceLandmarksRequest?
	private var processing = false

	private var captureSession = AVCaptureSession()
	private let settings = UserDefaults.standard

	private var scene = SKLevel(fileNamed: "Level")

	public var photos: [UIImage] = []

	public var levelDifficulty: Int = 1
	public var levelSpeed: CGFloat = 1.0
	public var levelAccelerate: Bool = true
	public var levelName = ""
	public var levelSong: String?

    private var inlineAd: MMInlineAd!
    private let bannerPlacementID = "footer"

	override func viewDidLoad() {
		super.viewDidLoad()

		if let tempImage = UIImage(named: "Planet") {
			photos.append(tempImage)
		}

		if PHPhotoLibrary.authorizationStatus() == .authorized {
			loadPhotos()
		}

		if let sceneView = view as? SKView {
			scene?.scaleMode = .fill
			scene?.backgroundColor = UIColor.black
			scene?.isPaused = true
			scene?.levelAccelerate = levelAccelerate
			scene?.levelDifficulty = levelDifficulty
			scene?.levelName = levelName
			scene?.levelSpeed = levelSpeed
			scene?.levelSong = levelSong
			scene?.reducedMotion = settings.bool(forKey: "reducedMotion")
			scene?.controller = self
			scene?.setTextures(images: photos)

			sceneView.showsFPS = false
			sceneView.ignoresSiblingOrder = true
			sceneView.isAsynchronous = true
			sceneView.presentScene(scene)
		}

		guard let frontVideoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else  {
			return
		}
		guard let frontCamera = try? AVCaptureDeviceInput(device: frontVideoDevice) else{
			return
		}

		let videoOutput = AVCaptureVideoDataOutput()
		videoOutput.alwaysDiscardsLateVideoFrames = true
		videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey:kCVPixelFormatType_32BGRA] as [String : Any]
		videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .userInitiated))

		guard captureSession.canAddInput(frontCamera) else {
			return
		}
		guard captureSession.canAddOutput(videoOutput) else {
			return
		}

		captureSession.beginConfiguration()
		captureSession.sessionPreset = .medium
		captureSession.addInput(frontCamera)
		captureSession.addOutput(videoOutput)
		captureSession.commitConfiguration()

		faceRequest = VNDetectFaceLandmarksRequest(completionHandler: faceHandler)
		faceRequest?.revision = VNDetectFaceLandmarksRequestRevision2
		faceRequest?.usesCPUOnly = false

		viewFace.strokeColor = UIColor.green.cgColor

		Exprestive.createMenuButton(v: btnClose)

        inlineAd = MMInlineAd(placementId: bannerPlacementID, adSize: .banner)!
        inlineAd.delegate = self
        inlineAd.refreshInterval = MMInlineAd.minimumRefreshInterval()

        adContainer!.addSubview(inlineAd.view)
        inlineAd.request(nil)

        view.bringSubviewToFront(btnClose)
        view.bringSubviewToFront(adContainer)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		captureSession.startRunning()
		scene?.isPaused = false
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

		_ = Globals.playMusic(song: nil, repeats: false)
		captureSession.stopRunning()
		scene?.isPaused = true
	}

	func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

		if processing {
			return
		}

		guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

		processing = true

		let bufferImage = CIImage(cvPixelBuffer: pixelBuffer)

		DispatchQueue.global(qos: .userInitiated).async {

			let handler = VNImageRequestHandler(ciImage: bufferImage, orientation: .left, options: [:])

			try? handler.perform([self.faceRequest!])
		}

	}

	private func faceHandler(request: VNRequest, error: Error?){

		guard let facesFound = request.results as? [VNFaceObservation] else {
			return
		}

		DispatchQueue.main.async {

			let size = self.viewFace.bounds.size

			if let face = facesFound.first {
				if let landmarks = face.landmarks {

					if let innerLips = landmarks.innerLips?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(innerLips)
					}
					if let outerLips = landmarks.outerLips?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(outerLips)
					}
					if let leftEye = landmarks.leftEye?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(leftEye)
					}
					if let rightEye = landmarks.rightEye?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(rightEye)
					}
					if let nose = landmarks.nose?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(nose)
					}

					if let leftEyebrow = landmarks.leftEyebrow?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(leftEyebrow)
					}
					if let rightEyebrow = landmarks.rightEyebrow?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(rightEyebrow)
					}
					if let faceContour = landmarks.faceContour?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(faceContour)
					}

					if let leftPupil = landmarks.leftPupil?.pointsInImage(imageSize: size) {
						self.viewFace.featuresCircled.append(leftPupil)
					}
					if let rightPupil = landmarks.rightPupil?.pointsInImage(imageSize: size) {
						self.viewFace.featuresCircled.append(rightPupil)
					}

					let roll: CGFloat = face.roll as? CGFloat ?? 0.0
					
					AnalyzeFace.set(landmarks: landmarks, angle: roll)
				}
			}

			self.viewFace.setNeedsDisplay()
			self.processing = false
		}
	}

	//Load up to 10 favorited photos from photo library
	private func loadPhotos(){

		let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .smartAlbumFavorites, options: nil)

		if let assetCollection = smartAlbums.firstObject {
			let fetchOptions = PHFetchOptions()
			fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
			let allPhotos = PHAsset.fetchAssets(in: assetCollection, options: fetchOptions)

			//Check how many favorited photos exist
			var count = allPhotos.count
			if count > 0 {
				photos.removeAll()
				//Max out photos to 10
				if count > 10 {
					count = 10
				}
				for i in 0..<count {
					if let uiImage = allPhotos[i].uiImage() {
						photos.append(uiImage)
					}
				}
			}
		}
		
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let paused = scene?.isPaused,
			let phase = scene?.phase
			else { return }

		if paused {
			scene?.isPaused = false
			Globals.musicPlayer?.play()
			viewBlur.isHidden = true

		} else if phase == .playing {

			scene?.isPaused = true
			Globals.musicPlayer?.pause()
			viewBlur.isHidden = false
		}
	}

    func viewControllerForPresentingModalView() -> UIViewController {
        return self
    }

/*	func inlineAd(_ ad: MMInlineAd, abortDidFailWithError error: Error) {
		print(error.localizedDescription)
	}

	func inlineAd(_ ad: MMInlineAd, requestDidFailWithError error: Error) {
		print(error.localizedDescription)
	}
*/
}

