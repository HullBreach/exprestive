//
//  AnalyzeFace.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/8/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import Foundation
import UIKit
import Vision

class AnalyzeFace {

	public static var expression = "Rest"

	public static var labelStates: UILabel?
	public static var labelExpression: UILabel?


	private static var baselineSet = false

	//Detect mouth opened/closed
	private static var mouthRatio = CGSize(width: 1.0, height: 0.0)
	//Detect eyes opened/closed
	private static var leftEyeRatio = CGSize(width: 4.0, height: 1.0)
	private static var rightEyeRatio = CGSize(width: 4.0, height: 1.0)
	//Detect brows raised
	private static var leftBrowRatio = CGSize(width: 1.0, height: 1.0) //Rectangle of eye and brow
	private static var rightBrowRatio = CGSize(width: 1.0, height: 1.0) //Rectangle of eye and brow
	//Detect head tilted
	private static var roll = CGFloat.pi

	private static var tiltState = "center" //left, center, right
	private static var leftBrowState = "middle" //down, middle, up
	private static var rightBrowState = "middle" //down, middle, up
	private static var mouthState = "closed" //closed, opened, wide
	private static var leftEyeState = "opened" //opened, closed
	private static var rightEyeState = "opened" //opened, closed

	private static var expressionList = ["Afraid", "Asleep", "Raise Brows", "Smile", "Tilt Head", "Wink", "Yawn", "Yell"]

	//Afraid: Brows up, eyes opened, mouth opened 2:3 - 1:2
	//Smile: Mouth opened, lips widened
	//Yell: Brows down, eyes closed
	//Yawn: Mouth opened 3:4-2:3, eyes closed 1:0 - 1:4
	//Asleep: Eyes closed 1:0 - 1:4
	//Sigh: Mouth partly opened 2:1 - 1:1
	//Tilt Head: top meridian left/right

	public static func randomExpression() -> String{
		return expressionList[Int.random(in: 0..<expressionList.count)]
	}

	public static func reset(){
		baselineSet = false
	}

	public static func set(landmarks: VNFaceLandmarks2D, angle: CGFloat){
		var states = ""

		let leftEye: [CGPoint] = landmarks.leftEye?.normalizedPoints ?? []
		let rightEye: [CGPoint] = landmarks.rightEye?.normalizedPoints ?? []
		var leftBrow: [CGPoint] = landmarks.leftEyebrow?.normalizedPoints ?? []
		var rightBrow: [CGPoint] = landmarks.rightEyebrow?.normalizedPoints ?? []
		let mouth: [CGPoint] = landmarks.innerLips?.normalizedPoints ?? []

		//Create regions around eyes and brows
		leftBrow.append(contentsOf: leftEye)
		rightBrow.append(contentsOf: rightEye)

		//Apply new coords
		leftEyeRatio = size(points: leftEye)
		rightEyeRatio = size(points: rightEye)
		leftBrowRatio = size(points: leftBrow)
		rightBrowRatio = size(points: rightBrow)
		mouthRatio = size(points: mouth)
		roll = angle

		setStates()

		if baselineSet {

			if tiltState == "left" {
				states += "Head Tilt: Left\n"
			} else if tiltState == "right" {
				states += "Head Tilt: Right\n"
			} else {
				states += "Head Tilt: Center\n"
			}

			if mouthState == "wide" {
				states += "Mouth: Wide Open\n"
			} else if mouthState == "closed" {
				states += "Mouth: Closed\n"
			} else {
				states += "Mouth: Opened\n"
			}

			if leftEyeState == "closed" {
				states += "Left Eye: Closed\n"
			} else {
				states += "Left Eye: Opened\n"
			}

			if rightEyeState == "closed" {
				states += "Right Eye: Closed\n"
			} else {
				states += "Right Eye: Opened\n"
			}

			if leftBrowState == "up" {
				states += "Left Eyebrow: Up\n"
			} else if leftBrowState == "middle" {
				states += "Left Eyebrow: Middle\n"
			} else {
				states += "Left Eyebrow: Down\n"
			}

			if rightBrowState == "up" {
				states += "Right Eyebrow: Up"
			} else if rightBrowState == "middle" {
				states += "Right Eyebrow: Middle"
			} else {
				states += "Right Eyebrow: Down"
			}

			if mouthState == "wide" && leftEyeState == "closed" && rightEyeState == "closed" {

				expression = "Yawn"

			} else if (leftEyeState == "closed" && rightEyeState == "opened") || leftEyeState == "opened" && rightEyeState == "closed" {

				expression = "Wink"

			} else if mouthState == "closed" && leftBrowState == "middle" && rightBrowState == "middle" && leftEyeState == "closed" && rightEyeState == "closed" {

				expression = "Asleep"

			} else if mouthState == "opened" && leftBrowState == "up" && rightBrowState == "up" {

				expression = "Smile"

			} else if mouthState == "closed" && leftBrowState == "up" && rightBrowState == "up" {

				expression = "Raise Brows"

			} else if mouthState == "wide" && leftBrowState == "up" && rightBrowState == "up" {

				expression = "Afraid"

			} else if mouthState == "closed" && leftBrowState == "down" && rightBrowState == "down" {

				expression = "Angry"

			} else if mouthState != "closed" && leftBrowState == "down" && rightBrowState == "down" {

				expression = "Yell"

			} else if tiltState == "left" || tiltState == "right" {

				expression = "Tilt Head"

			} else {

				expression = "Rest"

			}

			if labelStates != nil {
				DispatchQueue.main.async {
					self.labelStates?.text = states
				}
			}
			if labelExpression != nil {
				DispatchQueue.main.async {
					self.labelExpression?.text = "Expression: "+expression.uppercased()
				}
			}
		}

		baselineSet = true

	}

	private static func setStates(){
		if roll > 2.0 && roll < 2.8 {
			tiltState = "left"
		} else if roll < -2.0 && roll > -2.8 {
			tiltState = "right"
		} else if roll > 2.8 || roll < -2.8 {
			tiltState = "center"
		}

		switch mouthState {
		case "wide":
			if mouthRatio.height < mouthRatio.width * 0.25 {
				mouthState = "closed"
			} else if mouthRatio.height < mouthRatio.width * 0.75 {
				mouthState = "opened"
			}
		case "opened":
			if mouthRatio.height < mouthRatio.width * 0.25 {
				mouthState = "closed"
			} else if mouthRatio.height > mouthRatio.width {
				mouthState = "wide"
			}
		default:
			if mouthRatio.height > mouthRatio.width * 0.5 {
				mouthState = "opened"
			} else if mouthRatio.height > mouthRatio.width {
				mouthState = "wide"
			}
		}

		switch leftEyeState {
		case "closed":
			if leftEyeRatio.height > leftEyeRatio.width * 0.35 {
				leftEyeState = "opened"
			}
		default:
			if leftEyeRatio.height < leftEyeRatio.width * 0.25 {
				leftEyeState = "closed"
			}
		}

		switch rightEyeState {
		case "closed":
			if rightEyeRatio.height > rightEyeRatio.width * 0.35 {
				rightEyeState = "opened"
			}
		default:
			if rightEyeRatio.height < rightEyeRatio.width * 0.25 {
				rightEyeState = "closed"
			}
		}

		switch leftBrowState {
		case "up":
			if leftBrowRatio.height < leftBrowRatio.width * 0.8 {
				leftBrowState = "middle"
			} else if leftBrowRatio.height < leftBrowRatio.width * 0.6 {
				leftBrowState = "down"
			}
		case "down":
			if leftBrowRatio.height > leftBrowRatio.width * 0.85 {
				leftBrowState = "up"
			} else if leftBrowRatio.height > leftBrowRatio.width * 0.75 {
				leftBrowState = "middle"
			}
		default:
			if leftBrowRatio.height > leftBrowRatio.width * 0.9 {
				leftBrowState = "up"
			} else if leftBrowRatio.height < leftBrowRatio.width * 0.6 {
				leftBrowState = "down"
			}
		}

		switch rightBrowState {
		case "up":
			if rightBrowRatio.height < rightBrowRatio.width * 0.80 {
				rightBrowState = "middle"
			} else if rightBrowRatio.height < rightBrowRatio.width * 0.6 {
				rightBrowState = "down"
			}
		case "down":
			if rightBrowRatio.height > rightBrowRatio.width * 0.85 {
				rightBrowState = "up"
			} else if rightBrowRatio.height > rightBrowRatio.width * 0.75 {
				rightBrowState = "middle"
			}
		default:
			if rightBrowRatio.height > rightBrowRatio.width * 0.9 {
				rightBrowState = "up"
			} else if rightBrowRatio.height < rightBrowRatio.width * 0.6 {
				rightBrowState = "down"
			}
		}

	}

	private static func size(points: [CGPoint]) -> CGSize {
		var minX: CGFloat = 1.0
		var minY: CGFloat = 1.0
		var maxX: CGFloat = 0.0
		var maxY: CGFloat = 0.0

		for point in points {
			let tempPoint = point.applying(CGAffineTransform(rotationAngle: CGFloat.pi - roll))
			minX = min(tempPoint.x, minX)
			minY = min(tempPoint.y, minY)
			maxX = max(tempPoint.x, maxX)
			maxY = max(tempPoint.y, maxY)
		}

		return CGSize(width: maxX - minX, height: maxY - minY)
	}

}
