//Generate a series of diagonal "/" lines
//vec2 offset
//vec4 color

void main() {
	float fillFormula = dot(gl_FragCoord.xy, 0.05 * offset);
	fillFormula = sin(fillFormula);
	fillFormula *= 0.2;
	fillFormula += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + fillFormula, color.a);
}
