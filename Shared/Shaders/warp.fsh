//Generate a warp distortion
//vec2 offset
//vec4 color

void main(){
	vec2 radius = v_tex_coord - 0.5; //Distance from the center
	vec2 dist = 20.0 * radius;
	dist *= offset;
	vec2 coord1 = vec2(cos(dist.x), sin(dist.x));
	coord1 *= radius.x;
	vec2 coord2 = vec2(-sin(dist.y), cos(dist.y));
	coord2 *= radius.y;
	vec2 coord = 0.5 + coord1;
	coord += coord2;
	gl_FragColor = vec4(texture2D(u_texture, coord).rgb, color.a);
}

