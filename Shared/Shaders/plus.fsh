//Generate a series of + lines
//vec2 offset
//vec4 color

void main() {
	vec2 newOffset = 0.02 * gl_FragCoord.xy;
	newOffset += offset;

	float fillFormula = sin(newOffset.x);
	fillFormula += sin(newOffset.y);
	fillFormula *= 0.1;
	fillFormula += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + fillFormula, color.a);
}
