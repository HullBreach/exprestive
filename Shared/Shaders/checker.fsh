//Generate a checkboard
//vec2 offset
//vec4 color

void main() {
	float fillFormula = fract(sin(dot(gl_FragCoord.xy, 0.02)+offset.x)+cos(dot(vec2(-gl_FragCoord.x,gl_FragCoord.y), 0.02)+offset.y));
	float rand = 0.2 * fillFormula;
	rand += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + rand, color.a);
}
