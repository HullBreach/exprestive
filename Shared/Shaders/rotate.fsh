//
//  rotate.fsh
//  Exprestive
//
//  Created by Daniel Gump on 2/17/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

void main(void){
	vec2 radius = v_tex_coord - 0.5; //Distance from the center
	vec2 coord1 = vec2(offset.x, offset.y);
	coord1 *= radius.x;
	vec2 coord2 = vec2(-offset.y, offset.x);
	coord2 *= radius.y;
	vec2 coord = coord1 + coord2;
	coord *= 0.6;
	coord += 0.5;
	gl_FragColor = vec4(texture2D(u_texture, coord).rgb, color.a);
}

