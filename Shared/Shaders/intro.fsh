struct Light{
	vec3 center = vec3(0.0,0.0,0.0);
	lowp vec3 color = vec3(1.0,1.0,1.0);
};

struct Ray{
	vec3 pos = vec3(0.5,0.5,-600.0);
	vec3 dir;
	float dist = 0.0;
};

struct SphereCollision{
	float dist = -1.0;
	vec3 dir = vec3(0.0,0.0,0.0);
	vec2 coord;
};

lowp vec3 blend(lowp vec3 pixel, Light light, Ray ray, lowp vec3 color, lowp float ambiance){
	lowp vec3 norm = normalize(light.center - ray.pos);
	lowp float tilt = dot(ray.dir, norm) * 0.5;
	return mix(pixel, mix(color, light.color, tilt), ambiance);
}

Ray trace(Ray ray, vec3 dir, float dist){
	ray.pos += ray.dir * dist;
	ray.dir = dir;
	ray.dist += dist;
	return ray; //position, direction, (total) dist
}

SphereCollision collide(vec3 raypos, vec3 raydir, vec4 sphere, float dim){
	vec3 dist = raypos - sphere.xyz;
	float a = dot(raydir, raydir);
	float b = dot(dist, raydir) * 2.0;
	float c = dot(dist, dist) - sphere.w;
	float disc = b * b;
	disc -= a * c * 4.0;
	SphereCollision mycol;
	
	if(disc < 0.0){
		return mycol;
	}
	
	float sqdisc = sqrt(disc);
	float t = b + sqdisc;
	float over2a = 0.5 / a;
	t *= -over2a;
	
	if(t >= 0.0){
		vec3 dir = raydir * t;
		vec3 normal = -dir - dist;
		normal = normalize(normal);
		float normaly1 = normal.y * 0.5 + 0.5;
		float atanpi = atan(normal.x, normal.z) * 0.15915494 + dim + 1.0;
		
		mycol.dist = length(dir);
		mycol.dir = reflect(raydir, normal);
		mycol.coord = vec2(fract(atanpi), normaly1);
		return mycol;
	}
	
	t = sqdisc - b;
	t *= over2a;
	
	if(t >= 0.01){
		vec3 dir = raydir * t;
		vec3 normal = -dir - dist;
		normal = normalize(normal);
		float normaly1 = normal.y * 0.5 + 0.5;
		float atanpi = atan(normal.x, normal.z) * 0.15915494 + dim + 1.0;
		
		mycol.dist = length(dir);
		mycol.dir = reflect(raydir, normal);
		mycol.coord = vec2(fract(atanpi), normaly1);
		return mycol;
	}
	
	return mycol;
}

void main() {
	vec4 spheres[5]; //center.xyz, radius_squared
	spheres[0] = sphere1Coords;
	spheres[1] = sphere2Coords;
	spheres[2] = sphere3Coords;
	spheres[3] = sphere4Coords;
	spheres[4] = sphere5Coords;
	
	Light light;
	//light.center = vec3(0.0,0.0,0.0);//lightPosition;
	//light.color = vec3(1.0,1.0,1.0);//lightColor;
	
	float initray0 = dimensions.y / dimensions.x * 0.5;
	vec2 initray1 = gl_FragCoord.xy / dimensions.x;
	vec2 initray2 = vec2(0.5, initray0);
	initray1 -= initray2;
	
	Ray myRay;
	//myRay.pos = vec3(0.5,0.5,-600.0);
	myRay.dir = normalize(vec3(initray1, 1.0));
	//myRay.dist = 0.0;
	
	lowp float ambiance = 1.0; //Start with 100% lighting
	lowp vec3 pixel = vec3(0.0,0.0,0.0); //Start with ambiance
	lowp vec3 color = vec3(0.0,0.0,0.0);
	float dist = 8500.0;
	float dim = dimensions.z;

	SphereCollision sphereColTemp;
	SphereCollision sphereCol;

	for(lowp int reflections=4; reflections>=0; reflections--){
		lowp int index = -1;
		
		//Step through all the spheres to find the closest collision
		for(lowp int i=4; i>=0; i--){
			sphereColTemp = collide(myRay.pos, myRay.dir, spheres[i], dim);
			float d = sphereColTemp.dist;
			if(d>0.0 && d<dist){
				sphereCol = sphereColTemp;
				color = texture2D(u_texture, sphereCol.coord).rgb;
				dist = d;
				index = i;
			}
		}
		
		if(index == -1){
			break;
		}
		
		myRay = trace(myRay, sphereCol.dir, sphereCol.dist);
		ambiance *= 0.5;
		pixel = blend(pixel, light, myRay, color, ambiance);
	}
	
	gl_FragColor = vec4(pixel, 1.0);
}


