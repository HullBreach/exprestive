//Generate a series of vertical lines
//vec2 offset
//vec4 color

void main() {
	float fillFormula = gl_FragCoord.x * offset.y;
	fillFormula += offset.x;
	fillFormula = sin(fillFormula);
	fillFormula *= 0.2;
	fillFormula += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + fillFormula, color.a);
}
