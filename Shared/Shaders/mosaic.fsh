//Generate a series of colored lines
//vec2 offset
//vec4 color

void main() {
	vec2 newOffset = 0.02 * gl_FragCoord.xy;
	newOffset += offset;
	float fillFormula = cos(newOffset.x);
	fillFormula += sin(newOffset.y);
	fillFormula = fract(fillFormula);
	fillFormula *= 0.2;
	fillFormula += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + fillFormula, color.a);
}
