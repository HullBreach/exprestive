//
//  gameboy.fsh
//  Exprestive
//
//  Created by Daniel Gump on 2/17/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

void main(){
	vec2 dimensions = vec2(18.0, 32.0);
	vec2 box = 0.025 + v_tex_coord;
	box *= dimensions;

	float boxX = fract(box.x);
	float boxY = fract(box.y);
	if(boxX>=0.1*offset.x && boxX<=1.0-0.1*offset.x && boxY>=0.1*offset.y && boxY<=1.0-0.1*offset.y){
		vec2 coords = floor(box)/dimensions;
		vec2 margin = 0.05/dimensions;
		if(coords.x >= margin.x && coords.x <= 1.0-margin.x && coords.y >= margin.y && coords.y <= 1.0-margin.y){
			gl_FragColor = vec4(0.0, floor(6.0*dot(texture2D(u_texture, coords).rgb, vec3(0.299, 0.587, 0.114)))/6.0, 0.0, color.a);
		}else{
			gl_FragColor = vec4(0.25, 1.0, 0.0, color.a);
		}
	}else{
		gl_FragColor = vec4(0.25, 1.0, 0.0, color.a);
	}
}
