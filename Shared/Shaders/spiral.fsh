//Generate a spiral distortion
//vec2 offset
//vec4 color

void main(){
	vec2 radius = v_tex_coord - 0.5; //Distance from the center

	float dist = 0.7 - distance(v_tex_coord, vec2(0.5, 0.5));

	vec2 dist2 = offset - 0.5;
	dist2 *= 5.0;
	dist2 *= dist;

	float xDist = 0.8 * cos(dist2.x);
	float yDist = 0.8 * sin(dist2.y);

	float coordX = radius.x * xDist;
	coordX -= radius.y * yDist;
	float coordY = radius.x * yDist;
	coordY += radius.y * xDist;
	vec2 coord = 0.5 + vec2(coordX, coordY);
	gl_FragColor = vec4(texture2D(u_texture, coord).rgb, color.a);
}

/*

 vec2 coord = vec2(radius.x * xDist, radius.x * yDist);
 coord += vec2(-radius.y * yDist, radius.y * xDist);

 */
