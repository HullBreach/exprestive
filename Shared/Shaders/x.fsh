//Generate a series of X lines
//vec2 offset
//vec4 color

void main() {
	float fillFormula = dot(gl_FragCoord.xy, offset);
	fillFormula = sin(fillFormula);
	fillFormula += cos(dot(vec2(-gl_FragCoord.x,gl_FragCoord.y), offset));
	fillFormula *= 0.1;
	fillFormula += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + fillFormula, color.a);
}
