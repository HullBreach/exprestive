//
//  gameboy.fsh
//  Exprestive
//
//  Created by Daniel Gump on 2/17/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

void main(){
	vec2 dimensions = vec2(9.0 + 9.0*offset.x, 16.0 + 16.0*offset.y);
	vec2 box = 0.025 + v_tex_coord;
	box *= dimensions;

	if(fract(box.x)>=0.1 && fract(box.x)<=0.9 && fract(box.y)>=0.1 && fract(box.y)<=0.9){
		vec2 coords = floor(box)/dimensions;
		if(coords.x>=0.05/dimensions.x && coords.x<=1.0-0.05/dimensions.x && coords.y>=0.05/dimensions.y && coords.y<=1.0-0.05/dimensions.y){
			gl_FragColor = vec4(0.0, floor(6.0*dot(texture2D(u_texture, coords).rgb, vec3(0.299, 0.587, 0.114)))/6.0, 0.0, color.a);
		}else{
			gl_FragColor = vec4(0.25, 1.0, 0.0, color.a);
		}
	}else{
		gl_FragColor = vec4(0.25, 1.0, 0.0, color.a);
	}
}
