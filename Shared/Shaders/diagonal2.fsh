//Generate a series of diagonal "\" lines
//vec2 offset
//vec4 color

void main() {
	float fillFormula = dot(vec2(-gl_FragCoord.x,gl_FragCoord.y), 0.05 * offset);
	fillFormula = cos(fillFormula);
	fillFormula *= 0.2;
	fillFormula += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + fillFormula, color.a);
}
