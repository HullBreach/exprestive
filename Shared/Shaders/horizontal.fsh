//Generate a series of horizontal lines
//vec2 offset
//vec4 color

void main() {
	float fillFormula = gl_FragCoord.y * offset.x;
	fillFormula += offset.y;
	fillFormula = sin(fillFormula);
	fillFormula *= 0.2;
	fillFormula += 0.2;
	gl_FragColor = vec4(0.6 * color.rgb + fillFormula, color.a);
}

