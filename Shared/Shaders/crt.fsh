//Generate a CRT simulation
//vec2 offset
//vec4 color

void main(){
	vec2 radius = v_tex_coord - 0.5; //Distance from the center
	radius *= 0.8;

	vec2 newCoords = offset.x - 0.5;
	newCoords *= dot(radius, radius);
	newCoords += 1.0;
	newCoords *= radius;
	newCoords += 0.5;

	float line = 320.0 * newCoords.y;
	line += offset.y;
	line = mod(line, 2.0);
	line *= 0.5;

	gl_FragColor = vec4(line * texture2D(u_texture, newCoords).rgb, color.a);
}
