//
//  ViewFace.swift
//  SDK Paint
//
//  Created by Daniel Gump on 2/7/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import UIKit

class ViewFace: UIView {

	public var featuresOpened: [[CGPoint]] = []
	public var featuresClosed: [[CGPoint]] = []
	public var featuresCircled: [[CGPoint]] = []
	public var scale = CGSize(width: 1.0, height: 1.0)

	public var image: CGImage?

	public var strokeColor = UIColor.green.cgColor

	override func draw(_ rect: CGRect) {
		if let context = UIGraphicsGetCurrentContext() {

			context.setStrokeColor(strokeColor)
			context.setLineWidth(4.0)
			context.setLineCap(.round)
			context.setLineJoin(.round)

			if featuresOpened.count > 0 {
				for feature in featuresOpened {
					context.beginPath()
					context.move(to: feature[0])
					for p in 1..<feature.count {
						context.addLine(to: feature[p])
					}
					context.strokePath()
				}

				featuresOpened.removeAll()
			}

			if featuresClosed.count > 0 {
				for feature in featuresClosed {
					context.beginPath()
					context.move(to: feature[0])
					for p in 1..<feature.count {
						context.addLine(to: feature[p])
					}
					context.closePath()
					context.strokePath()
				}

				featuresClosed.removeAll()
			}

			if featuresCircled.count > 0 {
				for feature in featuresCircled {
					context.beginPath()
					context.addArc(center: feature[0], radius: 5.0, startAngle: 0.0, endAngle: CGFloat.pi*2.0, clockwise: false)
					context.strokePath()
				}

				featuresCircled.removeAll()
			}

			if featuresClosed.count > 0 || featuresOpened.count > 0 || featuresCircled.count > 0 {
				image = context.makeImage()
			}
		}

	}

}

