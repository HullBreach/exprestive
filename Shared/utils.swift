//
//  utils.swift
//  ML Image Indentifier
//
//  Created by Daniel Gump on 12/8/18.
//  Copyright © 2018 HullBreach Studios. All rights reserved.
//

import AVFoundation
import Foundation
import Photos
import UIKit

extension CMSampleBuffer {
	func image() -> CGImage? {
		// Get a CMSampleBuffer's Core Video image buffer for the media data
		guard let imageBuffer = CMSampleBufferGetImageBuffer(self) else { return nil }

		// Lock the base address of the pixel buffer
		CVPixelBufferLockBaseAddress(imageBuffer, CVPixelBufferLockFlags.readOnly)

		// Get the number of bytes per row for the pixel buffer
		let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)

		// Get the number of bytes per row for the pixel buffer
		let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)

		// Get the pixel buffer width and height
		let width = CVPixelBufferGetWidth(imageBuffer)
		let height = CVPixelBufferGetHeight(imageBuffer)

		// Create a device-dependent RGB color space
		let colorSpace = CGColorSpaceCreateDeviceRGB()

		// Create a bitmap graphics context with the sample buffer data
		var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Little.rawValue
		bitmapInfo |= CGImageAlphaInfo.premultipliedFirst.rawValue & CGBitmapInfo.alphaInfoMask.rawValue

		//let bitmapInfo: UInt32 = CGBitmapInfo.alphaInfoMask.rawValue
		let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)

		// Create a Quartz image from the pixel data in the bitmap graphics context
		let quartzImage = context?.makeImage()

		// Unlock the pixel buffer
		CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags.readOnly)

		// Create an image object from the Quartz image
		return quartzImage
	}

}

extension PHAsset {
	func uiImage() -> UIImage? {
		var img: UIImage?

		let options = PHImageRequestOptions()
		options.version = .original
		options.isSynchronous = true

		PHImageManager.default().requestImageData(for: self, options: options) { data, _, _, _ in
			if let data = data {
				img = UIImage(data: data)
			}
		}
		return img
	}
}
