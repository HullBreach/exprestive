//
//  SKIntro.swift
//  SDK Spriter
//
//  Created by Daniel Gump on 8/7/17.
//  Copyright © 2017 HullBreach Studios. All rights reserved.
//

import SpriteKit

class SKIntro: SKScene {
	
	public var panScale: Float = 0
	public var panFrame = 0

	private let scale = Float(UIScreen.main.scale)
	private var screenSize = UIScreen.main.bounds.size
	private var frameNum: Int = 0

	private var sphereZ:[Float] = [0.0, 0.0, 0.0, 0.0, 0.0]

	private var spriteBackground = SKSpriteNode()

	override func didMove(to view: SKView) {
		super.didMove(to: view)

		spriteBackground = (childNode(withName: "spriteBackground") as? SKSpriteNode)!
	}
	
	override func update(_ currentTime: TimeInterval) {

		frameNum = (frameNum + panFrame + 241) % 240
		panScale *= 0.9
		panFrame = Int(Float(panFrame) * 0.9)

		for i in 0...4 {
			sphereZ[i] = 100.0*Float(i)+600.0*sin(Float(frameNum+(48*i))/120.0*Float.pi)
		}

		spriteBackground.size = CGSize(width: 375.0, height: 812.0)
		spriteBackground.shader?.uniforms = [
			SKUniform(name: "dimensions", vectorFloat3: [scale*Float(screenSize.width),scale*Float(screenSize.height), Float(frameNum)/240.0]),
			SKUniform(name: "sphere1Coords", vectorFloat4: [-300.0,0.0,sphereZ[0], 40000.0*(1.0+panScale)]),
			SKUniform(name: "sphere2Coords", vectorFloat4: [300.0,200.0,sphereZ[1], 40000.0*(1.0+panScale)]),
			SKUniform(name: "sphere3Coords", vectorFloat4: [0.0,-450.0,sphereZ[2], 64000.0*(1.0+panScale)]),
			SKUniform(name: "sphere4Coords", vectorFloat4: [0.0,100.0,sphereZ[3], 20000.0*(1.0+panScale)]),
			SKUniform(name: "sphere5Coords", vectorFloat4: [400.0,-200.0,sphereZ[4], 90000.0*(1.0+panScale)])
		]
			
	}
}

