//
//  SKLevel.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/14/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import SpriteKit

class SKTitle: SKScene {

	private let shaders: [SKShader] = [
			SKShader(fileNamed: "diagonal1.fsh"),
			SKShader(fileNamed: "diagonal2.fsh"),
			SKShader(fileNamed: "plus.fsh"),
			SKShader(fileNamed: "x.fsh"),
			SKShader(fileNamed: "horizontal.fsh"),
			SKShader(fileNamed: "vertical.fsh")
	]

	private var spriteBackgrounds: [SKSpriteNode] = []

	private var screenSize = UIScreen.main.bounds.size

	private var frameNum = 0
	private var second = 0
	private var fade: Float = 0.0

	private let colors: [vector_float3] = [
		vector_float3(1.0, 0.5, 0.5),
		vector_float3(1.0, 0.75, 0.5),
		vector_float3(0.5, 1.0, 0.5),
		vector_float3(0.5, 1.0, 1.0),
		vector_float3(0.5, 0.5, 1.0)
	]

	private var color1 = vector_float3(0.0, 0.0, 0.0)
	private var color2 = vector_float3(0.0, 0.0, 0.0)

	public var reducedMotion = false

	override func didMove(to view: SKView) {
		super.didMove(to: view)

		if let spriteBackground1 = childNode(withName: "spriteBackground1") as? SKSpriteNode {
			spriteBackgrounds.append(spriteBackground1)
		}
		if let spriteBackground2 = childNode(withName: "spriteBackground2") as? SKSpriteNode {
			spriteBackgrounds.append(spriteBackground2)
		}

		color1 = colors[Int.random(in: 0..<colors.count)]
		color2 = colors[Int.random(in: 0..<colors.count)]
	}

	override func update(_ currentTime: TimeInterval) {
		super.update(currentTime)

		frameNum = (frameNum + 1) % 120

		if frameNum == 0 {
			second += 1
		}
		if second == 0 {
			fade = Float(frameNum) / 119.0
			if fade == 1.0 {
				spriteBackgrounds[0].shader = shaders[Int.random(in: 0..<shaders.count)]
				color1 = colors[Int.random(in: 0..<colors.count)]
			}
		}
		if second == 5 {
			fade = (119.0-Float(frameNum)) / 119.0
			if fade == 0.0 {
				spriteBackgrounds[1].shader = shaders[Int.random(in: 0..<shaders.count)]
				color2 = colors[Int.random(in: 0..<colors.count)]
			}
		}
		if second == 10 {
			second = 0
		}

		let angle: Float = reducedMotion ? 0.0 : Float(frameNum) * Float.pi / 60.0

		spriteBackgrounds[0].size = CGSize(width: 375.0, height: 812.0)
		spriteBackgrounds[0].alpha = 1.0
		spriteBackgrounds[0].shader?.uniforms = [
			SKUniform(name: "offset", vectorFloat2: [0.5+0.5*cos(angle),0.5+0.5*sin(angle)]),
			SKUniform(name: "color", vectorFloat4: [color1.x, color1.y, color1.z, 1.0])
		]

		spriteBackgrounds[1].size = CGSize(width: 375.0, height: 812.0)
		spriteBackgrounds[1].alpha = CGFloat(fade)
		//spriteBackgrounds[1].isHidden = (fade == 0.0)
		spriteBackgrounds[1].shader?.uniforms = [
			SKUniform(name: "offset", vectorFloat2: [0.5+0.5*cos(angle),0.5+0.5*sin(angle)]),
			SKUniform(name: "color", vectorFloat4: [color2.x, color2.y, color2.z, fade])
		]
	}

}
