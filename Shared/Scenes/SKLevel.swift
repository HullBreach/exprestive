//
//  SKLevel.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/14/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import CoreData
import SpriteKit
import UIKit

enum LevelPhase: Int {
	case introStart = 0
	case intro = 1
	case introEnd = 2
	case playing = 3
	case outroStart = 4
	case outro = 5
	case outroEnd = 6
}

class SKLevel: SKScene {

	private let shaders: [SKShader] = [
			SKShader(fileNamed: "diagonal1.fsh"),
			SKShader(fileNamed: "diagonal2.fsh"),
			SKShader(fileNamed: "plus.fsh"),
			SKShader(fileNamed: "x.fsh"),
			SKShader(fileNamed: "horizontal.fsh"),
			SKShader(fileNamed: "vertical.fsh"),
			SKShader(fileNamed: "rotate.fsh"),
			SKShader(fileNamed: "warp.fsh"),
			SKShader(fileNamed: "spiral.fsh"),
			SKShader(fileNamed: "crt.fsh"),
			SKShader(fileNamed: "gameboy2.fsh")
	]

	private var spriteBackgrounds: [SKSpriteNode] = []
	private var labelExpressions: [SKLabelNode] = []
	private var labelScore = SKLabelNode(text: "")
	private var labelHighScore = SKLabelNode(text: "")
	private var labelTimer = SKLabelNode(text: "2:00")
	private var labelLevelName = SKLabelNode(text: "Level Name")
	private var startedTime: TimeInterval = 0.0

	public var controller: UIViewController?

	public var score = 0

	public var levelAccelerate: Bool = true
	public var levelDifficulty: Int = 1
	public var levelName = ""
	public var levelSpeed: CGFloat = 1.0
	public var levelSong: String?

	public var phase: LevelPhase = LevelPhase.introStart
	private var phaseFrame: CGFloat = 0.0 //frames
	private var timer: TimeInterval = 120.0 //seconds

	private var screenSize = UIScreen.main.bounds.size //screen dimensions in points

	private var textures: [SKTexture] = []

	private var frameNum = 0
	private var second = 0
	private var fade: Float = 0.0

	private let colors: [vector_float3] = [
		vector_float3(1.0, 0.0, 0.0),
		vector_float3(1.0, 0.5, 0.0),
		vector_float3(0.0, 1.0, 0.0),
		vector_float3(0.0, 1.0, 1.0),
		vector_float3(0.0, 0.0, 1.0)
	]

	private var color1 = vector_float3(0.0, 0.0, 0.0)
	private var color2 = vector_float3(0.0, 0.0, 0.0)

	public var reducedMotion = false

	private let formatter = DateComponentsFormatter()

	override func didMove(to view: SKView) {
		super.didMove(to: view)

		let spriteBackground1 = childNode(withName: "spriteBackground1") as! SKSpriteNode
		spriteBackgrounds.append(spriteBackground1)
		let spriteBackground2 = childNode(withName: "spriteBackground2") as! SKSpriteNode
		spriteBackgrounds.append(spriteBackground2)

		labelScore = childNode(withName: "labelScore") as! SKLabelNode
		labelHighScore = childNode(withName: "labelHighScore") as! SKLabelNode
		labelTimer = childNode(withName: "labelTimer") as! SKLabelNode

		labelLevelName = childNode(withName: "labelLevelName") as! SKLabelNode
		labelLevelName.text = levelName

		let labelExpression = childNode(withName: "labelExpression") as! SKLabelNode
		labelExpressions.append(labelExpression)

		for _ in 1..<(2*levelDifficulty){
			let cloneExpression = labelExpression.copy() as! SKLabelNode
			labelExpressions.append(cloneExpression)
			addChild(cloneExpression)
		}

		for i in 0..<(2*levelDifficulty){
			let y = (1.0 + CGFloat(i) / CGFloat(levelDifficulty)) * -screenSize.height
			let expressionText = AnalyzeFace.randomExpression()

			labelExpressions[i].position.y = y
			labelExpressions[i].text = expressionText

			let lblExpChild = labelExpressions[i].children[0] as! SKLabelNode
			lblExpChild.text = expressionText
		}

		color1 = colors[Int.random(in: 0..<colors.count)]
		color2 = colors[Int.random(in: 0..<colors.count)]

		formatter.allowedUnits = [.minute, .second]
		formatter.unitsStyle = .positional
		formatter.zeroFormattingBehavior = .pad
	}

	override func update(_ currentTime: TimeInterval) {
		super.update(currentTime)

		frameNum = (frameNum + 1) % 60

		switch phase {
		case .introStart:
			startIntro()
		case .intro:
			animateIntro()
		case .introEnd:
			endIntro(currentTime: currentTime)
		case .outroStart:
			startOutro()
		case .outro:
			animateOutro()
		case .outroEnd:
			endOutro()
		default:
			animatePlaying(currentTime: currentTime)
		}

		let angle: Float = reducedMotion ? 0.0 : Float(frameNum) * Float.pi / 30.0

		spriteBackgrounds[0].size = CGSize(width: 375.0, height: 812.0)
		spriteBackgrounds[0].alpha = 1.0
		spriteBackgrounds[0].shader?.uniforms = [
			SKUniform(name: "offset", vectorFloat2: [0.5+0.5*cos(angle),0.5+0.5*sin(angle)]),
			SKUniform(name: "color", vectorFloat4: [color1.x, color1.y, color1.z, 1.0])
		]

		spriteBackgrounds[1].size = CGSize(width: 375.0, height: 812.0)
		spriteBackgrounds[1].alpha = CGFloat(fade)
		//spriteBackgrounds[1].isHidden = (fade == 0.0)
		spriteBackgrounds[1].shader?.uniforms = [
			SKUniform(name: "offset", vectorFloat2: [0.5+0.5*cos(angle),0.5+0.5*sin(angle)]),
			SKUniform(name: "color", vectorFloat4: [color2.x, color2.y, color2.z, fade])
		]
	}

	private func startIntro(){

		phaseFrame = 0.0
		phase = .intro
	}

	private func animateIntro(){

		if phaseFrame < 60.0 {

			//Do nothing during scene transition

		}else if phaseFrame < 120.0 { //60 - 120

			labelLevelName.alpha = (phaseFrame - 60.0) / 60.0
			labelLevelName.xScale = 3.0 - phaseFrame / 60.0
			labelLevelName.yScale = 3.0 - phaseFrame / 60.0

		}else if phaseFrame < 240.0 { //120 - 240

			//Do nothing while level name shows

		}else if phaseFrame < 300.0 { //240 - 300

			labelLevelName.alpha = (300.0 - phaseFrame) / 60.0
			labelLevelName.xScale = phaseFrame / 60.0 - 3.0
			labelLevelName.yScale = phaseFrame / 60.0 - 3.0

		}else{

			phase = .introEnd

		}

		phaseFrame += 1.0

	}

	private func endIntro(currentTime: TimeInterval){

		let duration = Globals.playMusic(song: levelSong, repeats: false)
		if duration > 0.0 {
			timer = duration + currentTime
			labelTimer.text = formatter.string(from: duration)
			labelTimer.isHidden = false
		}
		labelLevelName.isHidden = true
		phaseFrame = 0.0
		phase = .playing

	}

	private func animatePlaying(currentTime: TimeInterval){

		let remaining: TimeInterval = timer - currentTime


		if remaining <= 0.0 {
			labelTimer.text = formatter.string(from: 0.0)
			phase = .outroStart
			return
		}

		labelTimer.text = formatter.string(from: remaining)

		var scale: CGFloat = 1.0
		var color = UIColor.cyan
		let height = frame.height

		for i in 0..<labelExpressions.count {

			let lblExp = labelExpressions[i]
			let lblExpChild = lblExp.children[0] as! SKLabelNode

			if lblExp.position.y >= 150 && lblExp.position.y <= 250 && AnalyzeFace.expression == lblExp.text {

				scale = CGFloat.random(in: 1.1..<1.3)
				color = UIColor.yellow
				score += 1
				labelScore.text = "\(score)"
			}else{
				scale = 1.0
				color = UIColor.cyan
			}

			lblExp.setScale(scale)
			lblExpChild.setScale(scale)
			lblExpChild.fontColor = color

			lblExp.position.y += levelSpeed
			if lblExp.position.y > height {
				lblExp.position.y = -height
				let expressionText = AnalyzeFace.randomExpression()
				lblExp.text = expressionText
				lblExpChild.text = expressionText
			}
		}
		
		if frameNum == 0 {
			second += 1
			if levelAccelerate {
				levelSpeed += 0.02
			}
		}
		if second == 0 {
			fade = Float(frameNum) / 59.0
			if fade == 1.0 {
				if textures.count > 0 {
					spriteBackgrounds[0].texture = textures[Int.random(in: 0..<textures.count)]
				}
				spriteBackgrounds[0].shader = shaders[Int.random(in: 0..<shaders.count)]
				color1 = colors[Int.random(in: 0..<colors.count)]
			}
		}
		if second == 5 {
			fade = (59.0-Float(frameNum)) / 59.0
			if fade == 0.0 {
				if textures.count > 0 {
					spriteBackgrounds[1].texture = textures[Int.random(in: 0..<textures.count)]
				}
				spriteBackgrounds[1].shader = shaders[Int.random(in: 0..<shaders.count)]
				color2 = colors[Int.random(in: 0..<colors.count)]
			}
		}
		if second == 10 {
			second = 0
		}

	}

	private func startOutro(){

		if Exprestive.saveHighScore(level: levelName, score: score) {
			labelHighScore.isHidden = false
		}

		labelTimer.text = ""
		_ = Globals.playMusic(song: nil, repeats: false)

		for labelExpression in labelExpressions {
			labelExpression.isHidden = true
		}

		phaseFrame = 0.0
		phase = .outro

	}

	private func animateOutro(){

		if phaseFrame < 30.0 {

			let lerp: CGFloat = phaseFrame / 30.0

			labelScore.fontSize = 50.0 * (1.0 - lerp) + 100.0 * lerp
			labelScore.position.x = 170.0 * (1.0 - lerp) + 0.5 * labelScore.frame.width * lerp
			labelScore.position.y = 290.0 * (1.0 - lerp)

		}else if phaseFrame < 180.0 {

			//Do nothing while score shows

		}else{

			phase = .outroEnd

		}

		phaseFrame += 1.0

	}

	private func endOutro(){

		isPaused = true

		controller?.dismiss(animated: true, completion: nil)

	}

	public func setTextures(images: [UIImage]) {
		for i in 0..<images.count {
			textures.append(SKTexture(image: images[i]))
		}

	}

}
