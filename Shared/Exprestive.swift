//
//  SDKSpriterClass.swift
//  SDK Spriter
//
//  Created by Daniel Gump on 6/9/17.
//  Copyright © 2017 HullBreach Studios. All rights reserved.
//

import AVFoundation
import CoreData
import UIKit

class Exprestive {

	public static let levelList = ["Beginner", "Easy", "Medium", "Hard", "Expert", "Insane", "Blitz 1", "Blitz 2", "Blitz 3", "Blitz 4", "Blitz 5"]
	
	public static func showDialogExit(srcView: UIViewController){
		let alert = UIAlertController(title: NSLocalizedString("ExitToMenu", comment: ""), message: "", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default){(action) in srcView.dismiss(animated: true, completion: nil) })
		alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil))
		srcView.present(alert, animated: true)
	}

	public static func saveHighScore(level: String, score: Int) -> Bool {
		//print("\(level): \(score)")

		if score <= 0 {
			return false
		}

		let highScores = Exprestive.loadHighScores(level: level)
		let max = highScores.count
		var scoreSet = false

		//High score list filled, replace lowest score that was beaten
		if max >= 10 {
			for i in (0..<10).reversed() {
				//print(highScores[i].score)
				if highScores[i].score < score {
					highScores[i].score = Int32(score)
					scoreSet = true
					break
				}
			}

		//Insert new score
		}else{
			let newScore = NSEntityDescription.insertNewObject(forEntityName: "HighScore", into: Globals.managedContext!) as? HighScore
			newScore?.level = level
			newScore?.score = Int32(score)
			scoreSet = true
		}

		do {
			try Globals.managedContext?.save()
		} catch _ {
			//print(error)
			scoreSet = false
		}

		return scoreSet
	}

	public static func loadHighScores(level: String?) -> [HighScore] {

		var highScores: [HighScore] = []

		let highScoreFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "HighScore")
		if let levelName = level {
			highScoreFetch.predicate = NSPredicate(format: "level == %@", levelName)
		}
		highScoreFetch.sortDescriptors = [
			NSSortDescriptor(key: "score", ascending: false)
		]
		highScoreFetch.fetchLimit = 10

		do {
			highScores = try Globals.managedContext?.fetch(highScoreFetch) as! [HighScore]
		} catch _ {
			//print(error)
		}

		return highScores
	}

	public static func createMenuButton(v: UIView) {

		let cellBounds = v.bounds

		let layerOuter = CAGradientLayer()
		layerOuter.frame = CGRect(x: 7.0, y: 5.0, width: cellBounds.width-14.0, height: cellBounds.height-6.0)
		layerOuter.colors = [
			UIColor(white: 0.4, alpha: 1.0).cgColor,
			UIColor(white: 0.4, alpha: 1.0).cgColor,
			UIColor(white: 0.3, alpha: 1.0).cgColor,
			UIColor(white: 0.3, alpha: 1.0).cgColor
		]
		layerOuter.locations = [0.0, 0.48, 0.52, 1.0]
		layerOuter.startPoint = CGPoint(x: 0.0, y: 0.0)
		layerOuter.endPoint = CGPoint(x: 0.0, y: 1.0)
		layerOuter.cornerRadius = 20.0
		layerOuter.shadowColor = UIColor.black.cgColor
		layerOuter.shadowRadius = 2.0
		layerOuter.shadowOpacity = 0.5
		layerOuter.shadowOffset = CGSize(width: 0.0, height: 2.0)

		let layerMiddle = CALayer()
		layerMiddle.frame = CGRect(x: 7.0, y: 5.0, width: cellBounds.width-14.0, height: cellBounds.height-10.0)
		layerMiddle.backgroundColor = UIColor(white: 0.65, alpha: 1.0).cgColor
		layerMiddle.cornerRadius = 18.0
		layerMiddle.borderWidth = 2.0
		layerMiddle.borderColor = UIColor(white: 0.9, alpha: 1.0).cgColor

		let layerInner = CAGradientLayer()
		layerInner.frame = CGRect(x: 9.0, y: 9.0, width: cellBounds.width-18.0, height: cellBounds.height-16.0)
		layerInner.colors = [
			UIColor(red: 0.3, green: 0.4, blue: 1.0, alpha: 1.0).cgColor,
			UIColor(red: 0.3, green: 0.4, blue: 1.0, alpha: 1.0).cgColor,
			UIColor(red: 0.3, green: 0.4, blue: 0.95, alpha: 1.0).cgColor,
			UIColor(red: 0.3, green: 0.4, blue: 0.95, alpha: 1.0).cgColor
		]
		layerInner.locations = [0.0, 0.52, 0.56, 1.0]
		layerInner.startPoint = CGPoint(x: 0.0, y: 0.0)
		layerInner.endPoint = CGPoint(x: 0.0, y: 1.0)
		layerInner.cornerRadius = 16.0

		//let btnLayer: CALayer = v.subviews.count == 1 ? v.subviews[0].layer : v.layer
		let btnLayer = v.layer
		btnLayer.sublayers?.removeAll()
		btnLayer.addSublayer(layerOuter)
		btnLayer.addSublayer(layerMiddle)
		btnLayer.addSublayer(layerInner)
	}

	public static func createTableButton(v: UIView, width: CGFloat?) {

		let cellBounds = CGRect(x: 0.0, y: 0.0, width: width ?? v.bounds.width, height: v.bounds.height)
		let trans = CGAffineTransform(a: 1.0, b: 0.0, c: -0.2, d: 1.0, tx: 0.0, ty: 0.0)

		let layerOuter = CAGradientLayer()
		layerOuter.frame = CGRect(x: 7.0, y: 3.0, width: cellBounds.width-14.0, height: cellBounds.height-6.0)
		layerOuter.colors = [
			UIColor(white: 0.3, alpha: 1.0).cgColor,
			UIColor(white: 0.3, alpha: 1.0).cgColor,
			UIColor(white: 0.2, alpha: 1.0).cgColor,
			UIColor(white: 0.2, alpha: 1.0).cgColor
		]
		layerOuter.locations = [0.0, 0.48, 0.52, 1.0]
		layerOuter.startPoint = CGPoint(x: 0.0, y: 0.0)
		layerOuter.endPoint = CGPoint(x: 0.0, y: 1.0)
		layerOuter.cornerRadius = 8.0
		layerOuter.shadowColor = UIColor.black.cgColor
		layerOuter.shadowRadius = 2.0
		layerOuter.shadowOpacity = 0.5
		layerOuter.shadowOffset = CGSize(width: 0.0, height: 2.0)
		layerOuter.setAffineTransform(trans)

		let layerMiddle = CALayer()
		layerMiddle.frame = CGRect(x: 7.0, y: 3.0, width: cellBounds.width-14.0, height: cellBounds.height-10.0)
		layerMiddle.backgroundColor = UIColor.gray.cgColor
		layerMiddle.cornerRadius = 6.0
		layerMiddle.borderWidth = 2.0
		layerMiddle.borderColor = UIColor(white: 0.7, alpha: 1.0).cgColor
		layerMiddle.setAffineTransform(trans)

		let layerInner = CAGradientLayer()
		layerInner.frame = CGRect(x: 9.0, y: 7.0, width: cellBounds.width-18.0, height: cellBounds.height-16.0)
		layerInner.colors = [
			UIColor(white: 0.96875, alpha: 1.0).cgColor,
			UIColor(white: 0.96875, alpha: 1.0).cgColor,
			UIColor(white: 0.90625, alpha: 1.0).cgColor,
			UIColor(white: 0.90625, alpha: 1.0).cgColor
		]
		layerInner.locations = [0.0, 0.52, 0.56, 1.0]
		layerInner.startPoint = CGPoint(x: 0.0, y: 0.0)
		layerInner.endPoint = CGPoint(x: 0.0, y: 1.0)
		layerInner.cornerRadius = 4.0
		layerInner.setAffineTransform(trans)

		v.layer.sublayers?.removeAll()
		v.layer.addSublayer(layerOuter)
		v.layer.addSublayer(layerMiddle)
		v.layer.addSublayer(layerInner)
	}

}
