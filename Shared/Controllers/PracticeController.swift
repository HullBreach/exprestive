//
//  PracticeController.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/21/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import AVFoundation
import SpriteKit
import UIKit
import Vision

class PracticeController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

	@IBOutlet weak var viewFace: ViewFace!
    
	@IBOutlet weak var viewEffect: UIVisualEffectView!
    
	@IBOutlet weak var labelStates: UILabel!
	@IBOutlet weak var labelExpression: UILabel!
    
	@IBOutlet weak var btnClose: UIButton!

	@IBAction func clickedBtnClose(_ sender: UIButton) {

		dismiss(animated: true, completion: nil)
	}

	private var faceRequest: VNDetectFaceLandmarksRequest?
	private var processing = false

	private var captureSession = AVCaptureSession()

	override func viewDidLoad() {
		super.viewDidLoad()

		guard let frontVideoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else  {
			return
		}
		guard let frontCamera = try? AVCaptureDeviceInput(device: frontVideoDevice) else{
			return
		}

		let videoOutput = AVCaptureVideoDataOutput()
		videoOutput.alwaysDiscardsLateVideoFrames = true
		videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey:kCVPixelFormatType_32BGRA] as [String : Any]
		videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .userInitiated))

		guard captureSession.canAddInput(frontCamera) else {
			return
		}
		guard captureSession.canAddOutput(videoOutput) else {
			return
		}

		captureSession.beginConfiguration()
		captureSession.sessionPreset = .medium
		captureSession.addInput(frontCamera)
		captureSession.addOutput(videoOutput)
		captureSession.commitConfiguration()

		let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
		previewLayer.frame = view.bounds
		previewLayer.opacity = 0.3
		previewLayer.videoGravity = .resizeAspectFill
		view.layer.addSublayer(previewLayer)

		faceRequest = VNDetectFaceLandmarksRequest(completionHandler: faceHandler)
		faceRequest?.revision = VNDetectFaceLandmarksRequestRevision2
		faceRequest?.usesCPUOnly = false

		view.bringSubviewToFront(viewFace)
		view.bringSubviewToFront(viewEffect)
		view.bringSubviewToFront(btnClose)

		viewFace.strokeColor = UIColor.green.cgColor

		viewEffect.layer.borderColor = UIColor.black.cgColor
		viewEffect.layer.borderWidth = 1.0
		viewEffect.layer.cornerRadius = 5.0

		AnalyzeFace.labelStates = labelStates
		AnalyzeFace.labelExpression = labelExpression

		Exprestive.createMenuButton(v: btnClose)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		_ = Globals.playMusic(song: "Joplin_-_Maple_Leaf_Rag", repeats: true)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		captureSession.startRunning()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

		_ = Globals.playMusic(song: nil, repeats: false)

		captureSession.stopRunning()
	}

	func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

		if processing {
			return
		}

		guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

		processing = true

		let bufferImage = CIImage(cvPixelBuffer: pixelBuffer)

		DispatchQueue.global(qos: .userInitiated).async {
			let handler = VNImageRequestHandler(ciImage: bufferImage, orientation: .left, options: [:])

			try? handler.perform([self.faceRequest!])
		}

	}

	private func faceHandler(request: VNRequest, error: Error?){

		guard let facesFound = request.results as? [VNFaceObservation] else {
			return
		}

		DispatchQueue.main.async {

			let size = self.viewFace.bounds.size

			if let face = facesFound.first {
				if let landmarks = face.landmarks {

					if let innerLips = landmarks.innerLips?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(innerLips)
					}
					if let outerLips = landmarks.outerLips?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(outerLips)
					}
					if let leftEye = landmarks.leftEye?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(leftEye)
					}
					if let rightEye = landmarks.rightEye?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(rightEye)
					}
					if let nose = landmarks.nose?.pointsInImage(imageSize: size) {
						self.viewFace.featuresClosed.append(nose)
					}

					if let leftEyebrow = landmarks.leftEyebrow?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(leftEyebrow)
					}
					if let rightEyebrow = landmarks.rightEyebrow?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(rightEyebrow)
					}
					if let faceContour = landmarks.faceContour?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(faceContour)
					}

					if let noseCrest = landmarks.noseCrest?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(noseCrest)
					}
					if let medianLine = landmarks.medianLine?.pointsInImage(imageSize: size) {
						self.viewFace.featuresOpened.append(medianLine)
					}

					if let leftPupil = landmarks.leftPupil?.pointsInImage(imageSize: size) {
						self.viewFace.featuresCircled.append(leftPupil)
					}
					if let rightPupil = landmarks.rightPupil?.pointsInImage(imageSize: size) {
						self.viewFace.featuresCircled.append(rightPupil)
					}

					let roll: CGFloat = face.roll as? CGFloat ?? 0.0

					AnalyzeFace.set(landmarks: landmarks, angle: roll)
				}
			}

			self.viewFace.setNeedsDisplay()
			self.processing = false
		}
	}

}
