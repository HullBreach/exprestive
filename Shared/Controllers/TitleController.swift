//
//  TitleController.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/21/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import Photos
import SpriteKit
import UIKit

class TitleController: UIViewController {

	@IBOutlet weak var imgLogo: UIImageView!

	@IBOutlet var buttons: [UIButton]!
    
	@IBAction func clickedBtnPractice(_ sender: UIButton) {

		if AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
			Globals.showDialogMessage(srcView: self, title: "You must grant camera access to Exprestive to play practice mode.")
			return
		}

		performSegue(withIdentifier: "seguePractice", sender: self)
	}

	@IBAction func clickedBtnPlay(_ sender: UIButton) {

		if AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
			Globals.showDialogMessage(srcView: self, title: "You must grant camera access to Exprestive to play the core game.")
			return
		}

		performSegue(withIdentifier: "segueLevelSelection", sender: self)
	}

	@IBAction func clickedBtnHighScores(_ sender: UIButton) {

		performSegue(withIdentifier: "segueHighScores", sender: self)
	}

	@IBAction func clickedBtnSpecialNotes(_ sender: UIButton) {

		performSegue(withIdentifier: "segueSpecialNotes", sender: self)

	}

	@IBAction func btnTouchDown(_ sender: UIButton) {

		UIView.animate(
			withDuration: 0.25,
			animations: {
				sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
			}
		)

	}

	@IBAction func btnTouchUp(_ sender: UIButton) {

		UIView.animate(
			withDuration: 0.25,
			animations: {
				sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
			}
		)
	}

	private let settings = UserDefaults.standard

	private var scene = SKTitle(fileNamed: "Title")

	override func viewDidLoad() {
		super.viewDidLoad()

		NotificationCenter.default.addObserver(self, selector: #selector(TitleController.applicationActive), name: UIApplication.didBecomeActiveNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(TitleController.applicationInactive), name: UIApplication.willResignActiveNotification, object: nil)

		if let sceneView = view as? SKView {
			scene?.scaleMode = .fill
			scene?.backgroundColor = UIColor.black
			scene?.isPaused = true

			sceneView.showsFPS = false
			sceneView.ignoresSiblingOrder = true
			sceneView.isAsynchronous = true
			sceneView.presentScene(scene)
		}

		for button in buttons {
			Exprestive.createMenuButton(v: button)
		}
	}

	@objc func applicationActive() {
		startAnimations()
	}

	@objc func applicationInactive() {
		self.imgLogo.alpha = 0.0
		imgLogo.transform = CGAffineTransform(rotationAngle: -0.25)
	}

    override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		self.imgLogo.alpha = 0.0
		imgLogo.transform = CGAffineTransform(rotationAngle: -0.25)

		scene?.isPaused = false

		if !(Globals.musicPlayer?.url?.absoluteString.hasSuffix("Fillmore_-_The_Circus_Bee.m4a") ?? false)  {
			_ = Globals.playMusic(song: "Fillmore_-_The_Circus_Bee", repeats: true)
		}
    }

    override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		startAnimations()

		//Prompt for camera access
		if AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined {
			AVCaptureDevice.requestAccess(for: .video) { response in
				if response {
					//access granted
				}
			}
		}

		//Prompt for photo library access
		if PHPhotoLibrary.authorizationStatus() == .notDetermined {
			PHPhotoLibrary.requestAuthorization({status in
				if status == .authorized{
					//access granted
				}
			})
		}
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)

		scene?.isPaused = true
	}

	private func startAnimations() {

		scene?.reducedMotion = settings.bool(forKey: "reducedMotion")

		UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseIn], animations: {
			self.imgLogo.alpha = 1.0
		}, completion: nil)

		UIView.animate(withDuration: 1.5, delay: 0.0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
			self.imgLogo.transform = CGAffineTransform(rotationAngle: 0.25)
		}, completion: nil)

	}
}
