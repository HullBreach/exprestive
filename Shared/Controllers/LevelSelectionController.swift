//
//  LevelSelectionController.swift
//  Exprestive
//
//  Created by Daniel Gump on 3/9/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import SpriteKit
import UIKit

class LevelSelectionController: UIViewController {

	@IBOutlet weak var btnClose: UIButton!
	@IBOutlet weak var btnTraining: UIButton!

	@IBOutlet weak var viewTable: UIView!

	@IBOutlet weak var viewTips: UIView!
	@IBOutlet weak var labelTips: UILabel!

	@IBAction func clickedBtnClose(_ sender: UIButton) {
		dismiss(animated: true, completion: nil)
	}

	@IBAction func tappedTips(_ sender: Any) {

		labelTips.text = tips[Int.random(in: 0..<tips.count)]

	}


	private let settings = UserDefaults.standard

	private var scene = SKTitle(fileNamed: "Title")

	public var levelName = ""

	private let tips: [String] =
		["If you favorite images in your Photo Gallery, they will randomly show as animated wallpapers during gameplay.",
		 "Some facial expressions can be difficult to achieve. Be sure your face is centered in camera view.",
		"Practice Mode can be a great way to learn the combinations of gestures required for the facial expressions.",
		"Play Exprestive in a well-illumated area for the most accurate facial recognition.",
		"Winking can be a difficult gesture, since most people slightly close both eyelids when trying to close just one set.",
		"For maximum points, be sure to hold a gesture as long as the text is within the green request bar.",
		"If you need to pause a game, tap anywhere on the screen during gameplay. Tap outside the \"Quit\" button to resume.",
		"If your device supports 3D Touch, you can see the per-level high scores directly from this screen.",
		"To unlock a new level, you must play the previous level once and earn points."]

	override func viewDidLoad() {
		super.viewDidLoad()

		if let sceneView = view as? SKView {
			scene?.scaleMode = .fill
			scene?.backgroundColor = UIColor.black
			scene?.isPaused = true
			scene?.reducedMotion = settings.bool(forKey: "reducedMotion")

			sceneView.showsFPS = false
			sceneView.ignoresSiblingOrder = true
			sceneView.isAsynchronous = true
			sceneView.presentScene(scene)
		}

		if let tableController = storyboard?.instantiateViewController(withIdentifier: "tableLevelSelection") as? LevelSelectionTableController {


			addChild(tableController)

			if let table = tableController.tableView {
				table.frame = viewTable.bounds
				tableController.width = UIScreen.main.bounds.width - 20
				viewTable.clipsToBounds = true
				viewTable.addSubview(table)
			}
		}

		Exprestive.createMenuButton(v: btnClose)
		Exprestive.createMenuButton(v: btnTraining)

		viewTips.layer.frame = CGRect(x: 0.0, y: 0.0, width: viewTips.bounds.width, height: viewTips.bounds.height-2.0)
		viewTips.layer.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 0.75, alpha: 1.0).cgColor
		viewTips.layer.borderColor = UIColor(white: 0.7, alpha: 1.0).cgColor
		viewTips.layer.borderWidth = 1.5
		viewTips.layer.cornerRadius = 8.0
		viewTips.layer.shadowColor = UIColor.black.cgColor
		viewTips.layer.shadowRadius = 2.0
		viewTips.layer.shadowOpacity = 0.5
		viewTips.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)

		labelTips.text = tips[Int.random(in: 0..<tips.count)]
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		//Globals.createTextFrame(v: viewTips)
		scene?.isPaused = false

		if !(Globals.musicPlayer?.url?.absoluteString.hasSuffix("Fillmore_-_The_Circus_Bee.m4a") ?? false)  {
			_ = Globals.playMusic(song: "Fillmore_-_The_Circus_Bee", repeats: true)
		}
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)

		scene?.isPaused = true

	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		if let destination = segue.destination as? LevelController {
			switch levelName {
			case "Beginner":
				destination.levelAccelerate = true
				destination.levelDifficulty = 1
				destination.levelSpeed = 1.0
				destination.levelSong = "Gynt_-_Suite_1_Opus_1_Morning"

			case "Easy":
				destination.levelAccelerate = true
				destination.levelDifficulty = 1
				destination.levelSpeed = 1.5
				destination.levelSong = "Bach_-_Blessed_Is_the_Man"

			case "Medium":
				destination.levelAccelerate = true
				destination.levelDifficulty = 2
				destination.levelSpeed = 2.0
				destination.levelSong = "Rossini_-_William_Tell_Overture"

			case "Hard":
				destination.levelAccelerate = true
				destination.levelDifficulty = 3
				destination.levelSpeed = 2.5
				destination.levelSong = "Chabrier_-_Espana_Rhapsody_for_Orchestra"

			case "Expert":
				destination.levelAccelerate = true
				destination.levelDifficulty = 4
				destination.levelSpeed = 3.0
				destination.levelSong = "Mangore_-_Maxixe"

			case "Insane":
				destination.levelAccelerate = true
				destination.levelDifficulty = 5
				destination.levelSpeed = 3.5
				destination.levelSong = "Wagner_-_Die_Walkure_Grand_Fantasie"

			case "Blitz 1":
				destination.levelAccelerate = false
				destination.levelDifficulty = 1
				destination.levelSpeed = 5.0
				destination.levelSong = "Bizet_-_Carmen"

			case "Blitz 2":
				destination.levelAccelerate = false
				destination.levelDifficulty = 2
				destination.levelSpeed = 5.0
				destination.levelSong = "Holst_-_Opus_28_Fantasia_on_the_Dargason"

			case "Blitz 3":
				destination.levelAccelerate = false
				destination.levelDifficulty = 3
				destination.levelSpeed = 5.0
				destination.levelSong = "Mussorgsky_-_Night_on_Bald_Mountain"

			case "Blitz 4":
				destination.levelAccelerate = false
				destination.levelDifficulty = 4
				destination.levelSpeed = 5.0
				destination.levelSong = "Mozart_-_Marriage_of_Figaro_Overture"

			case "Blitz 5":
				destination.levelAccelerate = false
				destination.levelDifficulty = 5
				destination.levelSpeed = 5.0
				destination.levelSong = "Rimsky-Korsakov_-_Flight_of_the_Bumblebee"

			default:
				destination.levelAccelerate = false
				destination.levelDifficulty = 1
				destination.levelSpeed = 1.0
				destination.levelSong = nil
				
			}

			destination.levelName = levelName
		}

		_ = Globals.playMusic(song: nil, repeats: false)
		return
	}

}
