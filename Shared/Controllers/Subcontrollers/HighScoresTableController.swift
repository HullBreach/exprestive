//
//  HighScoresTableController.swift
//  Exprestive
//
//  Created by Daniel Gump on 3/3/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import CoreData
import UIKit

class HighScoresTableController: UITableViewController {

	public var level: String?

	private var rowHeight: CGFloat = 1.0
	private var highScores: [HighScore] = []
	private var training = false

	override func viewDidLoad() {
		super.viewDidLoad()

		if level?.hasPrefix("Training") ?? false {
			training = true
		} else {
			highScores = Exprestive.loadHighScores(level: level)
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		rowHeight = tableView.bounds.height / 10
		tableView.reloadData()
	}

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return max(highScores.count, 1)
	}

	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return rowHeight
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		let cell = tableView.dequeueReusableCell(withIdentifier: "cellHighScore", for: indexPath)
		let row = indexPath.row

		if row > highScores.count - 1 {
			if training {
				cell.textLabel?.text = ""
			} else {
				cell.textLabel?.text = "(No high scores yet)"
			}
			cell.detailTextLabel?.text = ""
		} else {
			cell.textLabel?.text = highScores[row].level
			cell.detailTextLabel?.text = "\(highScores[row].score)"
		}

		return cell
	}

}
