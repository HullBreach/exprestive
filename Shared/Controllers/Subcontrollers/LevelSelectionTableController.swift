//
//  LevelSelectionTableController.swift
//  Exprestive
//
//  Created by Daniel Gump on 3/9/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import CoreData
import UIKit

class LevelSelectionTableController: UITableViewController, UIViewControllerPreviewingDelegate {

	private let levelList: [String] = [
		"Beginner",
		"Easy",
		"Medium",
		"Hard",
		"Expert",
		"Insane",
		"Blitz 1",
		"Blitz 2",
		"Blitz 3",
		"Blitz 4",
		"Blitz 5"
	]

	private var levelAvailable: [Bool] = [
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false
	]

	public var width: CGFloat?

	override func viewDidLoad() {
		super.viewDidLoad()

		registerForPreviewing(with: self, sourceView: tableView)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		for i in 1..<levelList.count {
			let scores: [HighScore] = Exprestive.loadHighScores(level: levelList[i-1])
			if scores.count > 0 {
				levelAvailable[i] = true
			}
		}

		//print(levelAvailable)

		tableView.reloadData()
	}

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return levelList.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cellLevelSelection", for: indexPath)
		let row = indexPath.row

		cell.textLabel?.text = "\(levelList[row])"

		if !levelAvailable[row] {
			cell.textLabel?.alpha = 0.3
			cell.tag = 0
		}else{
			cell.textLabel?.alpha = 1.0
			cell.tag = 1
		}

		Exprestive.createTableButton(v: cell.contentView, width: width)
		return cell
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let cell = tableView.cellForRow(at: indexPath),
			let parentController = parent as? LevelSelectionController {

			if cell.tag > 0 {
				parentController.levelName = levelList[indexPath.row]
				parentController.performSegue(withIdentifier: "segueLevel", sender: parent)
			}

		}
	}

	/*@available(iOS 13.0, *)
	override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {

		if let cell = tableView.cellForRow(at: indexPath),
			let parentController = parent as? LevelSelectionController {

			if cell.tag > 0 {
				parentController.levelName = levelList[indexPath.row]
				parentController.performSegue(withIdentifier: "segueLevel", sender: parent)


			}

		}

		return nil
	}*/

	func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
		// First, get the index path and view for the previewed cell.
		guard let indexPath = tableView.indexPathForRow(at: location),
			let cell = tableView.cellForRow(at: indexPath),
			let labelText = cell.textLabel?.text
			else { return nil }

		if cell.tag == 0 {
			return nil
		}

		// Enable blurring of other UI elements, and a zoom in animation while peeking.
		previewingContext.sourceRect = cell.frame

		if let viewController = storyboard?.instantiateViewController(withIdentifier: "tableHighScores") as? HighScoresTableController {

			viewController.level = labelText

			return viewController
		}

		return nil
	}

	func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {

		//Do nothing

	}
	
}
