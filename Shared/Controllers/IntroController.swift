//
//  IntroController.swift
//  Exprestive
//
//  Created by Daniel Gump on 2/20/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import AVFoundation
import SpriteKit
import UIKit

class IntroController: UIViewController {

	@IBAction func panGesture(_ sender: UIPanGestureRecognizer) {
		let vel = sender.velocity(in: view)
		scene?.panFrame = Int(vel.x/256.0)
		scene?.panScale = Float(vel.y/2048.0)
	}
	
	private var scene = SKIntro(fileNamed: "Intro")

	override func viewDidLoad() {
		super.viewDidLoad()

		if let sceneView = view as? SKView {
			scene?.scaleMode = .fill
			scene?.backgroundColor = UIColor.black
			scene?.isPaused = true

			sceneView.showsFPS = false
			sceneView.ignoresSiblingOrder = true
			sceneView.isAsynchronous = true
			sceneView.presentScene(scene)
		}
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		_ = Globals.playMusic(song: "jingle", repeats: false)
		scene?.isPaused = false
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

		scene?.isPaused = true
		_ = Globals.playMusic(song: nil, repeats: false)
	}

	//Dummy functions to prevent memory leaks
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

	}
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

	}
	override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {

	}
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		view.isUserInteractionEnabled = false
		performSegue(withIdentifier: "segueTitle", sender: self)
	}

}
