//
//  HighScoresController.swift
//  Exprestive
//
//  Created by Daniel Gump on 3/2/19.
//  Copyright © 2019 HullBreach Studios. All rights reserved.
//

import SpriteKit
import UIKit

class HighScoresController: UIViewController {

	@IBOutlet weak var btnClose: UIButton!

	@IBOutlet weak var viewEffect: UIVisualEffectView!
	@IBOutlet weak var viewFrame: UIView!

	@IBOutlet weak var viewTable: UIView!

	@IBAction func clickedBtnClose(_ sender: UIButton) {

		dismiss(animated: true, completion: nil)
	}

	private let settings = UserDefaults.standard

	private var scene = SKTitle(fileNamed: "Title")

	override func viewDidLoad() {
		super.viewDidLoad()

		if let sceneView = view as? SKView {
			scene?.scaleMode = .fill
			scene?.backgroundColor = UIColor.black
			scene?.isPaused = true
			scene?.reducedMotion = settings.bool(forKey: "reducedMotion")

			sceneView.showsFPS = false
			sceneView.ignoresSiblingOrder = true
			sceneView.isAsynchronous = true
			sceneView.presentScene(scene)
		}

		viewEffect.layer.cornerRadius = 10.0
		viewEffect.layer.borderWidth = 1.0
		viewEffect.layer.borderColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5).cgColor

		

		if let tableController = storyboard?.instantiateViewController(withIdentifier: "tableHighScores") as? HighScoresTableController {

			addChild(tableController)

			if let table = tableController.tableView {
				viewTable.clipsToBounds = true
				viewTable.addSubview(table)
				table.frame = viewTable.bounds
				table.reloadData()
			}
		}

		Exprestive.createMenuButton(v: btnClose)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		scene?.isPaused = false

		if !(Globals.musicPlayer?.url?.absoluteString.hasSuffix("Fillmore_-_The_Circus_Bee.m4a") ?? false)  {
			_ = Globals.playMusic(song: "Fillmore_-_The_Circus_Bee", repeats: true)
		}
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)

		scene?.isPaused = true

	}

}
